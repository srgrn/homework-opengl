#include <stdlib.h>
#include <stdio.h>

#include "glut.h"
#include <Math.h>     // Needed for sin, cos
#define PI 3.14159265f

struct Mouse 
{
	int x;		/*	the x coordinate of the mouse cursor	*/
	int y;		/*	the y coordinate of the mouse cursor	*/
	int lmb;	/*	is the left button pressed?		*/
	int mmb;	/*	is the middle button pressed?	*/
	int rmb;	/*	is the right button pressed?	*/

	int xpress; /*	stores the x-coord of when the first button press occurred	*/
	int ypress; /*	stores the y-coord of when the first button press occurred	*/
};
typedef struct Mouse Mouse;
typedef struct Button Button; // define the name Button for the callback function
typedef void (*ButtonCallback)(Button* b); // callback function definition

struct Button 
{
	int   x;							/* top left x coord of the button */
	int   y;							/* top left y coord of the button */
	int   w;							/* the width of the button */
	int   h;							/* the height of the button */
	int	  state;						/* the state, 1 if pressed, 0 otherwise */
	int	  highlighted;					/* is the mouse cursor over the control? */
	unsigned char* label;						/* the text label of the button */
	ButtonCallback callbackFunction;	/* A pointer to a function to call if the button is pressed */
};
// will hold an RGB value
struct Color 
{
	float R;
	float G;
	float B;
};
typedef struct Color Color;
struct Cell
{
	int x;
	int y;
	int road;
	int tree;
	int vertical;

};
typedef struct Cell Cell; // define the name foe the cell 

//current window size
int winw = 640;
int winh = 480;

//current cell size
int cellw = 10;
int cellh = 10;
int rows = 0;
int cols = 0;
// function declarations 
void Font(void *font,unsigned char *text,int x,int y);
void ButtonRelease(Button *b,int x,int y);
void ButtonPress(int x,int y);
void ButtonPassive(int x,int y);
void ButtonDraw(Button *b);
void Init();
void Draw2D();
void Draw();
void MouseButton(int button,int state,int x, int y);
void MouseMotion(int x, int y);
void MousePassiveMotion(int x, int y);
void Resize(int w, int h);
int TestIfInArea(int startX,int startY,int height,int width,int clickX,int clickY);
int CheckClickInButton(Button* b,int x,int y);
void CheckClickInWorld(int x, int y);
void chnageLand(int centerX,int centerY,int amount);
void updateCell(Cell* c,int boardX,int boardY);
void BorderDraw();
void WorldDraw();
int main(int argc,char **argv);

void callback(Button* b)
{
	if(b->state) {
		printf("entering %s mode\n",b->label);
	}else {
		printf("leaving %s mode\n",b->label);
	}
}


// there can be only one mouse object
Mouse TheMouse = {0,0,0,0,0};
// Array of four buttons 
Button btnArr[4] = {
				{5,5, 100,25, 0,0, (unsigned char*)"Mountain",callback },
				{5,35, 100,25, 0,0, (unsigned char*)"Valley",callback },
				{5,65, 100,25, 0,0, (unsigned char*)"Road",callback },
				{5,95, 100,25, 0,0, (unsigned char*)"Tree",callback } };
// renaming the buttons for easy conversion
Button* ButtonMountain = &btnArr[0];
Button* ButtonValley = &btnArr[1];
Button* ButtonRoad = &btnArr[2];
Button* ButtonTree = &btnArr[3];

// creating a global for the world
Cell** TheWorld;

// draw a text using bitmap font
void Font(void *font,unsigned char *text,int x,int y)
{
	glRasterPos2i(x, y);

	while( *text != '\0' )
	{
		glutBitmapCharacter( font, *text );
		++text;
	}
}

// tests if a point is inside a button
int TestIfInArea(int startX,int startY,int height,int width,int clickX,int clickY) 
{
	if( clickX > startX      && 
		clickX < startX+width &&
		clickY > startY      && 
		clickY < startY+height ) {
			return 1;
	}
	return 0;
}
int CheckClickInButton(Button* b,int x,int y)
{
	if(b)
	{
		return TestIfInArea(b->x,b->y,b->h,b->w,x,y);
	}
	return 0;
}

void CheckClickInWorld(int x,int y)
{
	Cell* start = &TheWorld[0][0];
	if(TestIfInArea(start->x,start->y,cellh*cols,cellw*rows,x,y))
	{
		int locX = (x-start->x-5)/cellw;
		int locY = (y-start->y-5)/cellh;
		Cell* selected = &TheWorld[locX][locY];
		updateCell(selected,locX,locY);
		
	}
}
void chnageLand(int centerX,int centerY,int amount)
{
	for(int i=-1;i<2;i++)
	{
		for(int j=-1;j<2;j++)
		{
			if((i+centerX)>=0 && (i+centerX)<rows && (j+centerY)>=0 && (j+centerY)<cols)
			{
				printf("changing vertical in cell %d,%d\n",i+centerX,j+centerY);
				Cell* curr = &TheWorld[i+centerX][j+centerY];
				curr->vertical += amount;
			}
		}
	}
	Cell* curr = &TheWorld[centerX][centerY];
	curr->vertical += amount;
}
void updateCell(Cell* c,int boardX,int boardY)
{
	printf("working on cell %d,%d\n",boardX,boardY);
	if(ButtonMountain->state)
	{
		chnageLand(boardX,boardY,5);
	}
	if(ButtonValley->state)
	{
		chnageLand(boardX,boardY,-5);
	}
	if(ButtonRoad->state)
	{
		printf("adding road to cell %d,%d\n",boardX,boardY);
		c->road = 1;
		c->tree = 0;
	}
	if(ButtonTree->state && c->vertical>0)
	{
		printf("adding tree to cell %d,%d\n",boardX,boardY);
		c->tree = 1;
		c->road = 0;
	}

}
void ButtonRelease(Button *b,int x,int y)
{
	if(b) 
	{
		if( CheckClickInButton(b,TheMouse.xpress,TheMouse.ypress) && 
			CheckClickInButton(b,x,y) )
		{

			if (b->callbackFunction) {
				b->callbackFunction(b);

			}
		}
	}
}
void resetBtnArr()
{
	for(int i=0; i<4;i++)
	{
		btnArr[i].state = 0;
	}
}

void ButtonPress(int x,int y)
{
	for(int i = 0; i<4; i++)
	{
		Button* b = &btnArr[i];
		
		if( CheckClickInButton(b,x,y) )
		{
			if(b->state) {
				b->state = 0;
			}else{
				resetBtnArr();
				b->state = 1;

			}
		}
	}
}


void ButtonPassive(int x,int y)
{
	for(int i=0; i<4; i++)
	{
		Button* b = &btnArr[i];
		if( CheckClickInButton(b,x,y) ) // if inside button
		{
			if( b->highlighted == 0 ) { // if the button is not already highligthed
				b->highlighted = 1;
				glutPostRedisplay(); // redraw with highlight
			}
		}
		else if( b->highlighted == 1 ) // if the leaving the button and it is highlighted
		{
			b->highlighted = 0;
			glutPostRedisplay(); // redraw without highlight
		}
	}
}


void ButtonDraw(Button *b)
{
	int fontx;
	int fonty;

	if(b)
	{
		if (b->highlighted) 
			glColor3f(0.7f,0.7f,0.8f);
		else 
			glColor3f(0.6f,0.6f,0.6f);

		// draw background for the button.
		glBegin(GL_QUADS);
			glVertex2i( b->x     , b->y      );
			glVertex2i( b->x     , b->y+b->h );
			glVertex2i( b->x+b->w, b->y+b->h );
			glVertex2i( b->x+b->w, b->y      );
		glEnd();

		 // draw an outline around the button with width 3
		glLineWidth(3);

		if (b->state) 
			glColor3f(0.4f,0.4f,0.4f);
		else 
			glColor3f(0.8f,0.8f,0.8f);

		glBegin(GL_LINE_STRIP);
			glVertex2i( b->x+b->w, b->y      );
			glVertex2i( b->x     , b->y      );
			glVertex2i( b->x     , b->y+b->h );
		glEnd();

		if (b->state) 
			glColor3f(0.8f,0.8f,0.8f);
		else 
			glColor3f(0.4f,0.4f,0.4f);

		glBegin(GL_LINE_STRIP);
			glVertex2i( b->x     , b->y+b->h );
			glVertex2i( b->x+b->w, b->y+b->h );
			glVertex2i( b->x+b->w, b->y      );
		glEnd();

		glLineWidth(1);


		
		// Calculate the x and y coords for the text string in order to center it.
		fontx = b->x + (b->w - glutBitmapLength(GLUT_BITMAP_HELVETICA_10,b->label)) / 2 ;
		fonty = b->y + (b->h+10)/2;

		// this is to change the text look when the button is pressed
		if (b->state) {
			fontx+=2;
			fonty+=2;
		}

		if(b->highlighted)
		{
			glColor3f(0,0,0);
			Font(GLUT_BITMAP_HELVETICA_10,b->label,fontx,fonty);
			fontx--;
			fonty--;
		}

		glColor3f(1,1,1);
		Font(GLUT_BITMAP_HELVETICA_10,b->label,fontx,fonty);
	}
}
void BorderDraw()
{
		
	int x=110;
	int	y=5;
	int	w=winw-x-5;
	int	h=winh-y-5;
	glLineWidth(2);

	glColor3f(0.7f,0.2f,0.1f);
	glBegin(GL_LINE_STRIP);
		glVertex2i(x+w, y);
		glVertex2i(x, y);
		glVertex2i(x, y+h);
		glVertex2i(x+w, y+h);
		glVertex2i(x+w, y);
	glEnd();
}
void setColor(Cell* c)
{
	if(c->road)
	{
		glColor3f(0.5f,0.5f,0.5f);
		return;
	}
	if(c->tree)
	{
		glColor3f(0.38f,0.53f,0.44);
		return;
	}
	if(c->vertical<0)
	{
		glColor3f(0.0f,0.0f,1.0f); // this is blue color of water
		return;
	}
	glColor3f(0.0f,1.0f,0.0f);
	// maximum green (vertical = 0) is (0.0f,1.0f,0.0f)
	// maximum brown (vertical > 100) is (0.4f,0.0f,0.0f) which is mostly red

}
void WorldDraw()
{
	for(int i =0; i<rows;i++)
	{
		for(int j=0;j<cols;j++)
		{
			
			Cell* c = &TheWorld[i][j];
			// for some yet unknown reason it doesn't seem to work properly when in external function
			if(c->vertical<0)
			{
				glColor3f(0.0f,0.0f,1.0f); // this is blue color of water

			}else 
			{
				float green = 255- c->vertical/2;
				float red = c->vertical;
				glColor3f(red/255,green/255,0.0f); // this is blue color of water
			}
			glBegin(GL_QUADS);
				glVertex2i(c->x+cellw, c->y);
				glVertex2i(c->x, c->y);
				glVertex2i(c->x, c->y+cellh);
				glVertex2i(c->x+cellw, c->y+cellh);
				glVertex2i(c->x+cellw, c->y);
			glEnd();
			if(c->tree)
			{
				glBegin(GL_TRIANGLE_FAN);
					glColor3f(0.47f, 0.627f, 0.47f); // tree green
					glVertex2f((float)(c->x+cellw/2), (float)(c->y+cellh/2));       // Center of circle
					int numSegments = 100;
					GLfloat angle;
					for(double i = 0; i < 2 * PI; i += PI / 6) //<-- Change this Value
						glVertex3f((cos(i) * cellh/2)+c->x-cellw/4 , (sin(i) * cellh/2)+c->y-cellh/4, 0.0);
					glEnd();
				glEnd();
			}else if(c->road)
			{
				glColor3f(0.5f,0.5f,0.5f); // color of concrete
				glBegin(GL_QUADS);
				glVertex2i(c->x+cellw-1, c->y-1);
				glVertex2i(c->x-1, c->y-1);
				glVertex2i(c->x-1, c->y+cellh-1);
				glVertex2i(c->x+cellw-1, c->y+cellh-1);
				glVertex2i(c->x+cellw-1, c->y-1);
			glEnd();
			}
		}
	}
}

void Init()
{
	glEnable(GL_LIGHT0);
	// creating an array 
	int x=110;
	int	y=5;
	int	w=winw-x-5;
	int	h=winh-y-5;
	
	rows = w/cellw;
	cols = h/cellh;
	TheWorld = (Cell**)malloc(sizeof(Cell*)*rows);
	Color blue = {0.0f,0.0f,1.0f};
	for(int i=0;i<rows;i++)
	{
		*(TheWorld+i) = (Cell*)malloc(sizeof(Cell)*cols);
		for(int j=0;j<cols;j++)
		{
			Cell* curr = &TheWorld[i][j];
			curr->x = x+i*cellw;
			curr->y = y+j*cellh;
			curr->vertical = -1;
			curr->road = 0;
			curr->tree = 0;
		}
	}

}


void Draw2D()
{
	
}

void Draw()
{
	glClear( GL_COLOR_BUFFER_BIT |
			 GL_DEPTH_BUFFER_BIT );

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	/*
	 *	Set the orthographic viewing transformation
	 */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,winw,winh,0,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//draw each button in his turn 
	//TODO: change into a function going over the array
	ButtonDraw(ButtonMountain);
	ButtonDraw(ButtonRoad);
	ButtonDraw(ButtonValley);
	ButtonDraw(ButtonTree);
	//draw the border of the map
	BorderDraw();
	WorldDraw();
	glutSwapBuffers();
}




/*----------------------------------------------------------------------------------------
 *	\brief	This function is called whenever a mouse button is pressed or released
 *	\param	button	-	GLUT_LEFT_BUTTON, GLUT_RIGHT_BUTTON, or GLUT_MIDDLE_BUTTON
 *	\param	state	-	GLUT_UP or GLUT_DOWN depending on whether the mouse was released
 *						or pressed respectivly. 
 *	\param	x		-	the x-coord of the mouse cursor.
 *	\param	y		-	the y-coord of the mouse cursor.
 */
void MouseButton(int button,int state,int x, int y)
{
	/*
	 *	update the mouse position
	 */
	TheMouse.x = x;
	TheMouse.y = y;

	/*
	 *	has the button been pressed or released?
	 */
	if (state == GLUT_DOWN) 
	{
		/*
		 *	This holds the location of the first mouse click
		 */
		if ( !(TheMouse.lmb || TheMouse.mmb || TheMouse.rmb) ) {
			TheMouse.xpress = x;
			TheMouse.ypress = y;
		}

		/*
		 *	Which button was pressed?
		 */
		switch(button) 
		{
		case GLUT_LEFT_BUTTON:
			TheMouse.lmb = 1;
			ButtonPress(x,y);
			break;
		case GLUT_MIDDLE_BUTTON:
			TheMouse.mmb = 1;
			break;
		case GLUT_RIGHT_BUTTON:
			TheMouse.rmb = 1;
			break;
		}
	}
	else 
	{
		/*
		 *	Which button was released?
		 */
		switch(button) 
		{
		case GLUT_LEFT_BUTTON:
			TheMouse.lmb = 0;
			ButtonRelease(ButtonTree,x,y);
			ButtonRelease(ButtonRoad,x,y);
			ButtonRelease(ButtonValley,x,y);
			ButtonRelease(ButtonMountain,x,y);
			CheckClickInWorld(x,y);
			break;
		case GLUT_MIDDLE_BUTTON:
			TheMouse.mmb = 0;
			break;
		case GLUT_RIGHT_BUTTON:
			TheMouse.rmb = 0;
			break;
		}
	}

	/*
	 *	Force a redraw of the screen. If we later want interactions with the mouse
	 *	and the 3D scene, we will need to redraw the changes.
	 */
	glutPostRedisplay();
}

// while a button is held
void MouseMotion(int x, int y)
{
	MousePassiveMotion(x,y); // becouse it is the same at the mmoment 
	if(TheMouse.lmb) {
		CheckClickInWorld(x,y);
	}
	glutPostRedisplay(); // redraw is neccessery to update the screen
}

void MousePassiveMotion(int x, int y)
{
	int dx = x - TheMouse.x;
	int dy = y - TheMouse.y;

	TheMouse.x = x;
	TheMouse.y = y;

	ButtonPassive(x,y);

}

//  general function to change windows size on resizing 
void Resize(int w, int h)
{
	winw = w;
	winh = h;

	glViewport(0,0,w,h);
}

int main(int argc,char **argv)
{
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB|GLUT_DEPTH|GLUT_DOUBLE);
	glutInitWindowSize(winw,winh);
	glutInitWindowPosition(200,100);
	glutCreateWindow("Homework 1");

	glutDisplayFunc(Draw);
	glutReshapeFunc(Resize);
	glutMouseFunc(MouseButton);
	glutMotionFunc(MouseMotion); // mouse is moved with button pressed
	glutPassiveMotionFunc(MousePassiveMotion); // mouse is moved without buttons pressed

	Init();

	glutMainLoop();
}








